﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace GetirHackathonXamarin.Model
{
    public class Test : INotifyPropertyChanged
    {

        DateTime startDate_ = DateTime.Now;

        public DateTime startDate
        {
            get
            {
                return startDate_;
            }

            set
            {
                startDate_ = value;
                OnPropertyChanged();
            }
        }
        DateTime endDate_ = DateTime.Now;

        public DateTime endDate
        {
            get
            {
                return endDate_.Date;
            }

            set
            {
                endDate_ = value;
            }
        }

        int minCount_;

        public int minCount
        {
            get
            {
                return minCount_;
            }

            set
            {
                minCount_ = value;
                OnPropertyChanged();
            }
        }
        int maxCount_;

        public int maxCount
        {
            get
            {
                return maxCount_;
            }

            set
            {
                maxCount_ = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}