﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GetirHackathonXamarin.Model;
using Newtonsoft;
using Newtonsoft.Json;

namespace GetirHackathonXamarin.Service
{
    public class GetirService
    {
        public async Task<bool> getPostData(Test _test)
        {
            var jsonData = JsonConvert.SerializeObject(_test);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://getir-bitaksi-hackathon.herokuapp.com");
                var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                var resutl = await client.PostAsync("/searchRecords", content);
                if (resutl.IsSuccessStatusCode) return true;
                else return false;
            }
        }
    }
}