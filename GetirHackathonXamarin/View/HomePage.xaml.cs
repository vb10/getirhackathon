﻿using System;
using System.Collections.Generic;
using GetirHackathonXamarin.Model;
using GetirHackathonXamarin.Service;
using Xamarin.Forms;

namespace GetirHackathonXamarin.View
{
    public partial class HomePage : ContentPage
    {

        public HomePage()
        {
            InitializeComponent();

        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            var service = new GetirService();
            var btn = (Button)sender;
            var res = await service.getPostData((Test)btn.CommandParameter);
            if (res) await DisplayAlert("Başarılı", "İstek yapıldı", "Tamam");
            else await DisplayAlert("Başarılı", "İstek yapıldı", "Tamam");
        }
    }
}