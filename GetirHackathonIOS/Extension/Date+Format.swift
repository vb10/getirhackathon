//
//  Date+Format.swift
//  GetirHackathonIOS
//
//  Created by Veli Bacik on 01/02/2018.
//  Copyright © 2018 Veli Bacik. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    
    func getDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from: self)
        return dateString
    }
}
