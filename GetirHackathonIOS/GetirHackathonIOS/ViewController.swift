//
//  ViewController.swift
//  GetirHackathonIOS
//
//  Created by Veli Bacik on 01/02/2018.
//  Copyright © 2018 Veli Bacik. All rights reserved.
//

import UIKit
import SCLAlertView



class ViewController: UIViewController {
    
    @IBOutlet weak
    var startDate: UIDatePicker!
    @IBOutlet weak
    var endDate: UIDatePicker!
    @IBOutlet weak
    var minCount: UITextField!
    @IBOutlet weak
    var maxCount: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func sendAll(_ sender: Any) {

        //eğer ilk haliyle kaldıysa hata
        if startDate.date == endDate.date {
            SCLAlertView().showError("Hata", subTitle: "İsteğiniz gönderilemedi")
            return
            

        }
        guard
            let countMin = Int(minCount.text!)
            else {
                return
        }
        guard
            let countMax = Int(maxCount.text!)
            else {
                return
        }
        
        let endD = endDate.date.getDate()
        let startD = startDate.date.getDate()
        var model = Simple(startDate: startD, endDate: endD, minCount: countMin, maxCount: countMax)

        let service = BaseServiceClient()
        service.postDataGetir(data: model) {
            (result) in
            if !result {
                SCLAlertView().showError("Başarısız", subTitle: "İsteğiniz gönderilemedi")
                return
            }
            
            SCLAlertView().showSuccess("Başarılı", subTitle: "İstek tamamlandı")
            self.clear()
        }
    }
}
private extension ViewController {
    func clear() {
        self.startDate.date = Date()
        self.endDate.date =  Date()
        self.maxCount.text = ""
        self.minCount.text = ""
    }
}
