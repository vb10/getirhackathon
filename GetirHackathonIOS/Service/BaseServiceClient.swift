//
//  BaseServiceClient.swift
//  GetirHackathonIOS
//
//  Created by Veli Bacik on 01/02/2018.
//  Copyright © 2018 Veli Bacik. All rights reserved.
//

import Foundation
import Alamofire


class BaseServiceClient  {
    func postDataGetir(data : Simple , completion: @escaping ( _ status: Bool) -> Void){
        
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data.getJsonData()!, options : .allowFragments) as? Dictionary<String,Any>
            {
                let url = "https://getir-bitaksi-hackathon.herokuapp.com/searchRecords"
                Alamofire.request(url, method: .post, parameters: jsonArray,encoding: JSONEncoding.default, headers: nil).responseJSON {
                    response in
                    switch response.result {
                    case .success:
                         completion(true)
                        break
                    case .failure(let error):
                        print(error.localizedDescription)
                        completion(false)
                        break
                    }
                }
                
            } else {
                 completion(false)
                print("bad json")
            }
        } catch let error as NSError {
             completion(false)
            print(error)
        }
    }
    
    
  
}

