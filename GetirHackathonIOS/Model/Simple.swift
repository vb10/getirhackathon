//
//  Simple.swift
//  GetirHackathonIOS
//
//  Created by Veli Bacik on 01/02/2018.
//  Copyright © 2018 Veli Bacik. All rights reserved.
//

import Foundation

struct Simple : Codable{
    
    var startDate : String
    var endDate : String
    var minCount : Int
    var maxCount : Int
    
    
}
extension Simple {
    
    func getJsonData() -> Data? {
        
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        do {
            let jsonData = try jsonEncoder.encode(self)
            return jsonData
        } catch {
            
            print("Simple Error: 31")
            return nil
        }
        
    }
}
